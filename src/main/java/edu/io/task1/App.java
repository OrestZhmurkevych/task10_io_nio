package edu.io.task1;

import java.io.*;

public class App {


    public static void serializeCar() {
        Car car = new Car();
        car.setBrand("Ford");
        car.setModel("Focus");
        car.setType("sedan");
        car.setMaxSpeed(200);
        car.setFuelTankCapacity(55.0);
        try {
            FileOutputStream fileOut = new FileOutputStream("src/main/resources/car.serialized");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(car);
            out.close();
            fileOut.close();
            System.out.println("Serialized data is saved in car.serialized file");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deserializeCar() {
        Car car = null;
        try {
            FileInputStream fileIn = new FileInputStream("src/main/resources/car.serialized");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            car = (Car) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("\nDeserialized car: ");
        System.out.println("Brand: " + car.getBrand());
        System.out.println("Model: " + car.getModel());
        System.out.println("Type: " + car.getType());
        System.out.println("Max speed: " + car.getMaxSpeed());
        System.out.println("Fuel tank capacity: " + car.getFuelTankCapacity());
    }

    public static void main(String[] args) {

        serializeCar();
        deserializeCar();
    }
}
