package edu.io.task2;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;

import static edu.io.task2.bufferedReader.BufferedReaderOperations.readFileWithBufferedReaderAndReturnTime;
import static edu.io.task2.bufferedReader.BufferedReaderOperations.writeToFileWithBufferedReaderAndReturnTime;
import static edu.io.task2.usualReader.UsualReaderOperations.readFileWithUsualReaderAndReturnTime;
import static edu.io.task2.usualReader.UsualReaderOperations.writeToFileWithUsualReaderAndReturnTime;

public class App {

    private static final String FILE_NAME = "src/main/resources/EURO-CITI-55MB.txt";
    private static Logger logger = LogManager.getLogger("App");


    public static void main(String[] args) throws IOException {
        final long timeOfReadUsualReader = readFileWithUsualReaderAndReturnTime(FILE_NAME);
        final long timeOfReadBufferedReader = readFileWithBufferedReaderAndReturnTime(FILE_NAME);
        logger.info("Usual reader took " + timeOfReadUsualReader  + " miliseconds to execute");
        logger.info("Buffered reader took " + timeOfReadBufferedReader  + " miliseconds to execute");

        final long timeOfWriteUsualReader = writeToFileWithUsualReaderAndReturnTime();
        final double result = timeOfWriteUsualReader / 1000.0;
        final long timeOfWriteBufferedReader = writeToFileWithBufferedReaderAndReturnTime();
        final double bufferedResult = timeOfWriteBufferedReader / 1000.0;
        logger.info("Usual writer took " + result + " seconds to execute");
        logger.info("Buffered writer took " + bufferedResult + " seconds to execute");
    }


}