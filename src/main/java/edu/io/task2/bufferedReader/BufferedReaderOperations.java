package edu.io.task2.bufferedReader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BufferedReaderOperations {


    public static long readFileWithBufferedReaderAndReturnTime(String fileName) {
        long startTime = 0;
        try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(fileName), getSize());
             InputStreamReader inputReader = new InputStreamReader(inputStream)) {
            startTime = System.currentTimeMillis();
            while (inputReader.read() != -1) {
            }
            System.out.println("File was read");
        } catch (IOException e) {
            e.printStackTrace();
        }
        long finishTime = System.currentTimeMillis();
        return finishTime - startTime;
    }

    public static long writeToFileWithBufferedReaderAndReturnTime() throws IOException {
        File file = new File("src/main/resources/bufferedReaderOut.txt");

        long startTime = System.currentTimeMillis();
        try (FileWriter fileWriter = new FileWriter(file);
             BufferedWriter writer = new BufferedWriter(fileWriter)) {
            final List<String> stringList = prepareData();
            if (file.exists()) {
                file.delete();
            }
            for (String line : stringList) {
                writer.write(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        long finishTime = System.currentTimeMillis();
        return finishTime - startTime;
    }

    private static List<String> prepareData() {
        List<String> allLines = new ArrayList<>();
        String line = "In this paper the European Cities Platform for Online Transaction Services \n" +
                "(EURO-CITI) project is presented.";
        for (int i = 0; i < 1000000; i++) {
            allLines.add(line);
        }
        return allLines;
    }

    public static int getSize() {
        System.out.print("Enter the buffer size -->");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int result = 0;
        try {
            result = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
