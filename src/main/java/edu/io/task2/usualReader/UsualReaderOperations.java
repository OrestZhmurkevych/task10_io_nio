package edu.io.task2.usualReader;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsualReaderOperations {


    public static long readFileWithUsualReaderAndReturnTime(String fileName) {
        long startTime = System.currentTimeMillis();

        try (InputStream inputStream = new FileInputStream(fileName);
             InputStreamReader inputReader = new InputStreamReader(inputStream);) {
            while (inputReader.read() != -1) {
            }
            System.out.println("File was read");
        } catch (IOException e) {
            e.printStackTrace();
        }
        long finishTime = System.currentTimeMillis();
        return finishTime - startTime;
    }

    public static long writeToFileWithUsualReaderAndReturnTime() {
        final List<String> stringList = prepareData();
        File file = new File("src/main/resources/usualReaderOut.txt");
        if (file.exists()) {
            file.delete();
        }
        long startTime = System.currentTimeMillis();
        try (FileWriter writer = new FileWriter(file)) {
            for (String line : stringList) {
                writer.write(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        long finishTime = System.currentTimeMillis();
        return finishTime - startTime;
    }

    private static List<String> prepareData() {
        List<String> allLines = new ArrayList<>();
        String line = "In this paper the European Cities Platform for Online Transaction Services \n" +
                "(EURO-CITI) project is presented.";
        for (int i = 0; i < 1000000; i++) {
            allLines.add(line);
        }
        return allLines;
    }
}
