package edu.io.task7;

import java.io.*;
import java.net.Socket;

class ClientActions {

    private Socket socket;
    private BufferedReader in;
    private BufferedWriter out;
    private BufferedReader inputUser; // stream of reading from console
    private String addr; // ip address
    private int port; // port of connection
    private String nickname;


    public ClientActions(String addr, int port) { //address and port for creation
        this.addr = addr;
        this.port = port;
        try {
            this.socket = new Socket(addr, port);
        } catch (IOException e) {
            System.err.println("Socket failed");
        }
        try {
            // streams of reading from socket/writing to socket and reading from console
            inputUser = new BufferedReader(new InputStreamReader(System.in));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.pressNickname(); // asks nickname
            new ReadMsg().start(); // thread that reads message from socket
            new WriteMsg().start(); // thread that writes message in socket from console
        } catch (IOException e) {
            ClientActions.this.downService();
        }
    }

    private void pressNickname() {
        System.out.print("Press your nickname: ");
        try {
            nickname = inputUser.readLine();
            out.write("Hello " + nickname + "\n");
            out.flush();
        } catch (IOException ignored) {
        }

    }

    private void downService() { //turn off the socket
        try {
            if (!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
            }
        } catch (IOException ignored) {
        }
    }

    // thread of reading messages from server
    private class ReadMsg extends Thread {
        @Override
        public void run() {

            String str;
            try {
                while (true) {
                    str = in.readLine(); // wait for message from server
                    if (str.equals("stop")) {
                        ClientActions.this.downService();
                        break;
                    }
                    System.out.println(str); // write message from server on console
                }
            } catch (IOException e) {
                ClientActions.this.downService();
            }
        }
    }

    // thread that sends messages from console to server
    public class WriteMsg extends Thread {

        @Override
        public void run() {
            while (true) {
                String userWord;
                try {
                    userWord = inputUser.readLine(); // message from console
                    if (userWord.equals("stop")) {
                        out.write("stop" + "\n");
                        ClientActions.this.downService();
                        break;
                    } else {
                        out.write(nickname + ": " + userWord + "\n"); // send to server
                    }
                    out.flush();
                } catch (IOException e) {
                    ClientActions.this.downService();

                }

            }
        }
    }
}


public class Client {

    public static String ipAddr = "localhost";
    public static int port = 8080;


    public static void main(String[] args) { //creates client connection
        new ClientActions(ipAddr, port);
    }
}
