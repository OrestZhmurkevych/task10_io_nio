package edu.io.task7;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;


class ServerThread extends Thread {

    private Socket socket; // socket, which allow the server and the client to communicate
    private BufferedReader in;
    private BufferedWriter out;


    public ServerThread(Socket socket) throws IOException { //socket for the communication
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        start(); // executes run()
    }

    @Override
    public void run() {
        String word;
        try {
            // first message is a nickname
            word = in.readLine();
            try {
                out.write(word + "\n");
            } catch (IOException ignored) {}
            try {
                while (true) {
                    word = in.readLine();
                    if(word.equals("stop")) {
                        this.downService(); // shut of the server
                        break;
                    }
                    System.out.println("Echoing: " + word);
                    for (ServerThread vr : Server.serverList) {
                        vr.send(word);
                    }
                }
            } catch (NullPointerException ignored) {}
        } catch (IOException e) {
            this.downService();
        }
    }

    private void send(String msg) { //send of one message
        try {
            out.write(msg + "\n");
            out.flush();
        } catch (IOException ignored) {}

    }

    private void downService() { //shuts the server: cuts itself as a thread and deletes from threads list
        try {
            if(!socket.isClosed()) {
                socket.close();
                in.close();
                out.close();
                for (ServerThread vr : Server.serverList) {
                    if(vr.equals(this)) vr.interrupt();
                    Server.serverList.remove(this);
                }
            }
        } catch (IOException ignored) {}
    }
}


public class Server {

    public static final int PORT = 8080;
    public static LinkedList<ServerThread> serverList = new LinkedList<>(); // list of all threads of server which
                                                                                    // listen their clients

    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(PORT);
        System.out.println("Server Started");
        try {
            while (true) {
                Socket socket = server.accept();
                try {
                    serverList.add(new ServerThread(socket));
                } catch (IOException e) {
                    socket.close();
                }
            }
        } finally {
            server.close();
        }
    }
}